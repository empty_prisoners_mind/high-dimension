import numpy as np
import matplotlib.pyplot as plt
import json
# d 维数
d = 1000
# N 样本数
N = 10
mean = np.zeros((1,d))[0]
# mean = np.ones((1,d))[0]
cov = np.identity(d)

a = np.random.multivariate_normal(mean,cov,N)
b = np.random.multivariate_normal(mean,cov,N)
c = np.linalg.norm(a,ord=2,axis = 1)
d = np.linalg.norm(b,ord=2,axis = 1)
# c = np.dot(a,a) # 矩阵点积
e = np.matmul(a,np.transpose(b))
with open("1.txt",'w') as f:
    f.write(json.dumps(str(e)))
for i in range(N):
    for j in range(N):
        e[i][j] = e[i][j] / (c[i]*d[j])


x = np.arange(N)
plt.plot(x,e[0],label='Frist line',linewidth=3,color='r',marker='o',markerfacecolor='blue',markersize=12)
plt.xlabel('point index')
plt.ylabel('Cos Value')
# plt.title('Interesting Graph\nCheck it out')
plt.legend()
plt.show()
