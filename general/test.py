# 测试路径
# import sys
# print(sys.path)clear
# with open("dataset/1.txt",'w') as f:
#     f.write("aaaa")

# 测试散点图
# import matplotlib.pyplot as plt
# years = [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019]
# turnovers = [0.5, 9.36, 52, 191, 350, 571, 912, 1027, 1682, 2135, 2684]
# plt.figure(figsize=(10, 15), dpi=100)
# plt.scatter(years, turnovers, c='red', s=100, label='sdf')
# plt.scatter([2008],0.8,c='green', s=100, label='gf')
# plt.scatter([2007],0.6,c='green', s=100)
# plt.xticks(range(2008, 2020, 1))
# plt.yticks(range(0, 3200, 200))
# plt.xlabel("年份", fontdict={'size': 16})
# plt.ylabel("成交额", fontdict={'size': 16})
# plt.title("历年天猫双11总成交额", fontdict={'size': 20})
# plt.legend(loc='best')
# plt.show()

# 测试numpy

import numpy as np
a = np.array([[1,2,3],[4,5,6]])
b = np.array([[4,5,6],[1,2,3]])

def euclidean_distances(x, y, squared=True):
    """Compute pairwise (squared) Euclidean distances.
    """
    assert isinstance(x, np.ndarray) and x.ndim == 2
    assert isinstance(y, np.ndarray) and y.ndim == 2
    assert x.shape[1] == y.shape[1]

    x_square = np.sum(x*x, axis=1, keepdims=True)
    if x is y:
        y_square = x_square.T
    else:
        y_square = np.sum(y*y, axis=1, keepdims=True).T
    distances = np.dot(x, y.T)
    # use inplace operation to accelerate
    distances *= -2
    distances += x_square
    distances += y_square
    # result maybe less than 0 due to floating point rounding errors.
    np.maximum(distances, 0, distances)
    if x is y:
        # Ensure that distances between vectors and themselves are set to 0.0.
        # This may not be the case due to floating point rounding errors.
        distances.flat[::distances.shape[0] + 1] = 0.0
    if not squared:
        distances = np.sqrt(distances)
    return distances

print(euclidean_distances(a,a,squared=False))