import numpy as np
import matplotlib.pyplot as plt
# d 维数
d = 10000
# N 样本数
N = 10
mean = np.zeros((1,d))[0]
# mean = np.ones((1,d))[0]
cov = np.identity(d)

# mean = mean + 2
# cov = cov * 16

a = np.random.multivariate_normal(mean,cov,N)

b = np.linalg.norm(a,ord=2,axis = 1) / d
x = np.arange(N)
plt.plot(x,b,label='Frist line',linewidth=3,color='r',marker='o',markerfacecolor='blue',markersize=12)
plt.xlabel('point index')
plt.ylabel('2-Norm Value')
# plt.title('Interesting Graph\nCheck it out')
plt.legend()
plt.show()
