import numpy as np
import matplotlib.pyplot as plt
# d 维数
d = 1000
# N 样本数
N = 10

a = np.random.randint(0,1000,(N,d))

b = np.linalg.norm(a,ord=2,axis = 1)
x = np.arange(N)
plt.plot(x,b,label='Frist line',linewidth=3,color='r',marker='o',markerfacecolor='blue',markersize=12)
plt.xlabel('point index')
plt.ylabel('2-Norm Value')
# plt.title('Interesting Graph\nCheck it out')
plt.legend()
plt.show()
